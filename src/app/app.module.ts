import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';

import { AuthenticationModule } from './authentication/authentication.module'
import { DashboardModule } from './dashboard/dashboard.module'
import { AppRoutingModule } from './/app-routing.module';

import { ConfigService } from './services/config.service';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AuthenticationModule,
    DashboardModule,
    AppRoutingModule
  ],
  providers: [
    ConfigService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
