import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { ConfigService } from './services/config.service';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  token

  constructor(
    private authService: AuthService,
    public configService: ConfigService,
    private router: Router
  ) { 
    this.isUserLoggedIn()
  }

  onActivate(event) {
    window.scroll(0,0);
  }

  isUserLoggedIn(){
    this.token = JSON.parse(localStorage.getItem('token'))
    if(this.token){
      this.authService.response.next({token: this.token})
    }
  }
}
