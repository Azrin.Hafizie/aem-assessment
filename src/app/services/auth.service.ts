import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { resolve } from 'url';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  redirectUrl: string;
  isLoggedIn = false;

  response = new BehaviorSubject<object>({token: undefined})
  $response = this.response.asObservable()

  constructor(
    private http: HttpClient,    
    public configService: ConfigService,
  ) { }

  async login(data) {
    
    let param = {
      email: data.email,
      password: data.password
    }
    
    let header = await this.configService.setHeader('json')
    return new Promise((resolve, reject)=>{
      this.http.post(this.configService.baseUrl+'/auth/login', param, {headers: header})
        .subscribe((res:any)=>{
          let data = res
  
          if(data.success){
            localStorage.setItem('token', JSON.stringify(data.token)); 
            this.isLoggedIn = true
            this.response.next({ token: data.token })
          }

          resolve(res)
        },
        err=>{
          console.log('err', err)
          this.isLoggedIn = false
          this.response.next({token: undefined})
          reject(err)
        })
    })
  }

  async logOut(){
    localStorage.removeItem('token')
    return new Promise((resolve, reject)=>{
      let token = JSON.parse(localStorage.getItem('token'))
      if(!token){
        resolve(true)
        this.response.next({token: undefined})
      }else{
        reject(false)
      }
    })
  }

}
