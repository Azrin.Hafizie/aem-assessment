import { Component, OnInit } from '@angular/core';

import { DashboardService } from './../../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  chartDonut = []
  chartBar = []
  users = []

  pie = {
    showLegend: false,
    colorScheme: {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    },
    showLabels: true,
    explodeSlices: false,
    doughnut: true
  }

  bar = {
    showXAxis: true,
    showYAxis: true,
    gradient: false,
    showLegend: false,
    showXAxisLabel: true,
    xAxisLabel: 'Country',
    showYAxisLabel: true,
    yAxisLabel: 'Visits',
    colorScheme: {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    }
  }

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.getData()
  }

  getData(){
    this.dashboardService.getDashboardData()
    .then((res:any)=>{
      let data = res

      this.users = data.tableUsers

      data.chartDonut.forEach(function(e:any) {
        e.name = e.country;
        e.value = e.litres;
        delete e.country;    
        delete e.litres;    
      });
      this.chartDonut = data.chartDonut

      data.chartBar.forEach(function(e:any) {
        e.name = e.country;
        e.value = e.visits;
        delete e.country;    
        delete e.visits;    
      });
      this.chartBar = data.chartBar


    })
    .catch(err=>{
      console.log(err)
    })
  }
  

}
