import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading = {
    login: false
  }
  userForm: FormGroup;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    public router: Router,
  ) { }

  ngOnInit() {
    this.createLoginForm()
    this.isLoggedIn()
  }

  isLoggedIn(){
    let token = JSON.parse(localStorage.getItem('token'))
    if(token){
      this.router.navigate(['/dashboard']);
    }    
  }

  createLoginForm(){
    this.userForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      // email: ['admin@mail.com', Validators.compose([Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')])],
      // password: ['admin123', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  login(user){
    this.loading.login = true
    let data = user.value
    
    this.authService.login(data)
    .then((res:any)=>{
        this.loading.login = false
        if(res.success){
          this.router.navigate(['/dashboard']);
        }
    })
    .catch(err=>{
      console.log(err)
    })
  }

}
